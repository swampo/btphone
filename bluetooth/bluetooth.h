/***************************************************************************
*   Copyright (C) 2016 by Riccardo Redaelli, Fabiano Riccardi,            *
*   Andrea Salmoiraghi, Andrea Turri                                      *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   As a special exception, if other files instantiate templates or use   *
*   macros or inline functions from this file, or you compile this file   *
*   and link it with other works to produce a work based on this file,    *
*   this file does not by itself cause the resulting work to be covered   *
*   by the GNU General Public License. However the source code for this   *
*   file must still be made available in accordance with the GNU General  *
*   Public License. This exception does not invalidate any other reasons  *
*   why a work based on this file might be covered by the GNU General     *
*   Public License.                                                       *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, see <http://www.gnu.org/licenses/>   *
***************************************************************************/

#ifndef BLUETOOTH_H
#define BLUETOOTH_H

#include <string>
#include <fstream>
#include "miosix.h"
#include "led/led.h"
#include <tr1/functional>

using namespace std::tr1;

/**
 * Class Bluetooth manager to interact with bluetooth module ST SPBT2632C2A
 * with AT command set
 */
class Bluetooth
{
public:
        /*
         * Get the singleton
         */
        static Bluetooth& instance();
        /*
         * Initialize the bluetooth.
         * Open the file ot the UART to communicate with the bluetooth.
         * Set the callback mode to STRING.
         * @param auxPath the serial file.
         * @param cback the callback function to wich pass received string.
         */
        void init(const std::string& auxPath,function<void (std::string)> cback);
        /*
         * Initialize the bluetooth.
         * Open the file ot the UART to communicate with the bluetooth.
         * Set the callback mode to BUFFER.
         * @param auxPath the serial file.
         * @param cback the callback function to wich pass received buffer.
         * @param buffSize the buffer size
         */
        void init(const std::string& auxPath,function<void (const unsigned char*, unsigned int)> cback, unsigned int buffSize);

        /**
         * Start the connection.
         * Launch two threads: one for upcoming connection, one for the button push
         * to launch the discovery and try a new connection.
         */
        void connect();
        /**
         * Disconnect the bluetooth
         */
        void disconnect();
        /**
         * Reset the bluetooth module.
         * The bluetooth will be in command mode and the current connection will be down.
         */
        void reset();
        /**
         * True if the connection is active
         */
        bool isConnected();
        /*
         * True if is in command mode
         */
        bool isCommandMode();
        /*
         * True if is in bypass mode
         */
        bool isBypassMode();
        /*
         * Send command to enter in command mode
         */
        void sendCommandMode();
        /*
         * Send command to enter in bypass mode
         */
        void sendBypassMode();
        /*
         * Send a string
         * @param data the string to sent
         */
        void sendString(const std::string& data);
        /*
         * Send a buffer
         * @param sendBuffer the buffer
         * @param size the buffer size
         */
        void sendBuffer(const unsigned char* sendBuffer, unsigned int size);

private:
        Bluetooth();       // Bluetooth is a singleton, the constructor is private
        virtual ~Bluetooth();

        void initBT(const string& auxPath);
        std::string getLineString();
        void getBuffer();
        void waitingForButton();
        void waitingForRead();
        void interpreter(std::string& data);
        int getNumDiscoveredDevices(const std::string& data);
        std::string getAddressDiscoveredDevice(const std::string& data);
        std::string getLocalAddress(const std::string& data);
        static void* doWaitingForRead(void* arg);
        static void* doWaitingForButton(void* arg);
        static void* callbackLauncher(void* arg);
        void execCallback();

        FastMutex discoveryMutex;
        FastMutex connectionMutex;
        ConditionVariable connectionCV;
        ConditionVariable buttonWaitCV;

        unsigned int bufferSize;
        unsigned char *receivedBufferProcessing;
        unsigned char *receivedBufferReady;

        enum BluetoothState {
                NOT_CONNECTED,
                COMMAND_MODE,
                BYPASS_MODE
        };
        BluetoothState state;
        enum CallbackMode {
                STRING,
                BUFFER
        };
        CallbackMode callbackMode;

        Led& led;
        bool isDiscovering;

        function<void (const unsigned char*, unsigned int)> callbackBuffer;
        function<void (std::string)> callbackString;

        int sread;
        ofstream swrite;

        //String constants
        static const string BT_NAME;
        static const string BD_ADDRESS;
        static const string AT_LOCAL_NAME;
        static const string AT_DISCOVERY;
        static const string AT_SPP_CONNECT;
        static const string AT_SPP_DISCONNECT;
        static const string CONNECTION_UP;
        static const string INQ_PENDING;
        static const string DISCOVERY_PENDING;
        static const string DEVICE;
        static const string CONNECTION_DOWN;
        static const string COMMAND;
        static const string AT_BYPASS;
        static const string BYPASS;
        static const string ESCAPE;
};

#endif //BLUETOOTH_H
