/***************************************************************************
*   Copyright (C) 2016 by Riccardo Redaelli, Fabiano Riccardi,            *
*   Andrea Salmoiraghi, Andrea Turri                                      *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   As a special exception, if other files instantiate templates or use   *
*   macros or inline functions from this file, or you compile this file   *
*   and link it with other works to produce a work based on this file,    *
*   this file does not by itself cause the resulting work to be covered   *
*   by the GNU General Public License. However the source code for this   *
*   file must still be made available in accordance with the GNU General  *
*   Public License. This exception does not invalidate any other reasons  *
*   why a work based on this file might be covered by the GNU General     *
*   Public License.                                                       *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, see <http://www.gnu.org/licenses/>   *
***************************************************************************/

#include <iostream>
#include <fstream>
#include <fcntl.h>
#include "bluetooth.h"
#include "button/button.h"

using namespace std;
using namespace miosix;

#ifdef DEBUG
#define debug(str) do { std::cout << "[BT] " << str << std::endl; } while( false )
#else
#define debug(str) do { } while ( false )
#endif

typedef Gpio<GPIOA_BASE,1> resetBluetooth;

//String constants
const string Bluetooth::BT_NAME = "btPhone";
const string Bluetooth::BD_ADDRESS = "AT-AB BDAddress ";
const string Bluetooth::AT_LOCAL_NAME = "AT+AB LocalName ";
const string Bluetooth::AT_DISCOVERY = "AT+AB Discovery";
const string Bluetooth::AT_SPP_CONNECT = "AT+AB SPPConnect ";
const string Bluetooth::AT_SPP_DISCONNECT = "AT+AB SPPDisconnect ";
const string Bluetooth::CONNECTION_UP = "ConnectionUp";
const string Bluetooth::INQ_PENDING = "AT-AB InqPending";
const string Bluetooth::DISCOVERY_PENDING = "AT-AB DiscoveryPending ";
const string Bluetooth::DEVICE = "AT-AB Device ";
const string Bluetooth::CONNECTION_DOWN = "AT-AB ConnectionDown";
const string Bluetooth::COMMAND = "AT-AB -CommandMode-";
const string Bluetooth::AT_BYPASS = "AT+AB Bypass";
const string Bluetooth::BYPASS = "AT-AB -BypassMode-";
const string Bluetooth::ESCAPE = "^#^$^%";

//INITIALIZATION LIST
Bluetooth::Bluetooth() : state(NOT_CONNECTED), callbackMode(STRING), led(Led::instance()), isDiscovering(false)  {
}


void Bluetooth::init(const string& auxPath,function<void (string)> cback){
        debug("Initialization...");
        debug("Callback mode: STRING");
        callbackMode = STRING;
        callbackString=cback;
        state = NOT_CONNECTED;
        led.startBlink(kLedRed);
        reset();
        initBT(auxPath);
        led.turnOn(kLedRed);
        debug("Init done!");
}

void Bluetooth::init(const string& auxPath,function<void (const unsigned char*, unsigned int)> cback, unsigned int buffSize){
        //NOTE: The input buffer from serial has max lenght 32760
        if(buffSize>32760) {
                cout<<"[BLUETOOTH] Error: buffer size max is 32760"<<endl;
                exit(0);
        }
        debug("Initialization...");
        debug("Callback mode: BUFFER");
        callbackMode = BUFFER;
        bufferSize = buffSize;
        receivedBufferProcessing = (unsigned char *) malloc(sizeof(char)*bufferSize);
        receivedBufferReady = (unsigned char *) malloc(sizeof(char)*bufferSize);
        callbackBuffer=cback;
        state = NOT_CONNECTED;
        led.startBlink(kLedRed);
        reset();
        initBT(auxPath);
        led.turnOn(kLedRed);
        debug("Init done!");
}

bool Bluetooth::isConnected(){
        return state!=NOT_CONNECTED;
}

bool Bluetooth::isCommandMode(){
        return state==COMMAND_MODE;
}

bool Bluetooth::isBypassMode(){
        return state==BYPASS_MODE;
}

void Bluetooth::sendString(const string& data) {
        debug("Sending string:" << data);
        swrite << data << endl;
}

void Bluetooth::sendBuffer(const unsigned char* sendBuffer, unsigned int size) {
        swrite.write((const char*)sendBuffer, size);
        swrite.flush();
}

void Bluetooth::sendCommandMode(){
        debug("Entering in COMMAND MODE");
        swrite.flush();
        swrite << ESCAPE;
        swrite.flush();
        delayMs(2500);
}

void Bluetooth::sendBypassMode(){
        debug("Sending escape to enter in COMMAND MODE");
        swrite << AT_BYPASS << endl;
}

void Bluetooth::disconnect() {
        if(callbackMode==BUFFER){
            free(receivedBufferProcessing);
            free(receivedBufferReady);
        }
        callbackMode=STRING;
        debug("Disconnecting...");
        sendCommandMode();
        //If still in bypass mode
        if(!isCommandMode()) {
                debug("ERROR: not entered in command mode");
                return;
        }
        swrite << AT_SPP_DISCONNECT << endl;
        led.turnOff(kLedBlue);
        led.turnOff(kLedGreen);
        led.turnOff(kLedOrange);
        led.startBlink(kLedRed);
        delayMs(1700);
        led.turnOff(kLedRed);
        debug("Disconnected successfully!");
}

string Bluetooth::getLocalAddress(const string& data) {
        //String format for data: "AT-AB BDAddress [BD addr]"
        //"BT addr" is long 12 bytes
        string address = data.substr(BD_ADDRESS.length(),12);
        return address;
}

int Bluetooth::getNumDiscoveredDevices(const string& data) {
        //String format for data: "AT-AB DiscoveryPending [n]"
        string numStr = data.substr(DISCOVERY_PENDING.length());
        return atoi(numStr.c_str());
}

string Bluetooth::getAddressDiscoveredDevice(const string& data) {
        //String format for data: "AT-AB Device [BD addr] [name]"
        //"BT addr" is long 12 bytes
        //"name" is the name of device quoted
        string address = data.substr(DEVICE.length(),12);
        return address;
}

/*
 * This function interprete the string received from the bluetooth.
 * If it is a command execute the related instructions,
 * otherwise send the string to the callback function.
 */
void Bluetooth::interpreter(string& data){
        debug("Interpreting: " << data);
        if(state==BYPASS_MODE && data.find(COMMAND)!=string::npos) {
                debug("COMMAND mode active!");
                state=COMMAND_MODE;
                return;
        }
        if(state==BYPASS_MODE) {
                debug("Passing data to callback");
                callbackString(data);
                return;
        }
        if(state==COMMAND_MODE  && data.find(BYPASS)!=string::npos) {
                debug("BYPASS mode active!");
                state=BYPASS_MODE;
                return;
        }
        if(state==NOT_CONNECTED && data.find(CONNECTION_UP)!=string::npos) {
                debug("New connection upcoming");
                //Ignore response connection up
                getLineString();
                state=BYPASS_MODE;
                connectionCV.broadcast();
                return;
        }
        if(state==NOT_CONNECTED && data.find(INQ_PENDING)!=string::npos) {
                debug("Discovery inq pending");
                // After discovery
                data = getLineString();
                int numDevices = getNumDiscoveredDevices(data);
                debug("Discovered " << numDevices << " devices");
                string btPhoneAddr = "";
                for(int i=0; i<numDevices; i++) {
                        data = getLineString();
                        //When a btPhone is found, get the address and exit the for
                        if(data.find(BT_NAME)!=string::npos) {
                                btPhoneAddr = getAddressDiscoveredDevice(data);
                                debug("btPhone address: " << btPhoneAddr);
                                break;
                        }
                }
                if(btPhoneAddr!="") {
                        debug("btPhone found!");
                        debug("Trying connect it...");
                        led.startAnimation(kLedAnimationAllBlink);
                        swrite << AT_SPP_CONNECT << btPhoneAddr << endl;
                        data = getLineString();
                        if(data.find(CONNECTION_UP)!=string::npos) {
                                debug("Connection ok");
                                //BypassMode message
                                getLineString();
                                state=BYPASS_MODE;
                        }else{
                                debug("Error during connection!");
                        }
                } else {
                        debug("WARNING: btPhone not found!");
                        led.stopAnimation();
                        led.turnOn(kLedRed);
                }
                isDiscovering=false;
                buttonWaitCV.broadcast();
                return;
        }
        if(data.find(CONNECTION_DOWN)!=string::npos) {
                debug("Connection down!");
                state=NOT_CONNECTED;
                led.turnOn(kLedRed);
                led.turnOff(kLedBlue);
                return;
        }
}
/*
 * This function receive all the incoming bytes and decide what to do wrt the current callback mode;
 */
void Bluetooth::waitingForRead(){
        debug("Function waiting for read active!");
        bool first = true;
        pthread_t callbackThread;
        string data;
        while(1) {
                if(state!=BYPASS_MODE || callbackMode==STRING) {
                        data = getLineString();
                        interpreter(data);
                }
                else if(callbackMode==BUFFER) {
                        getBuffer();
                        // swaps the ready and the processing buffer: allows double buffering
                        //on the callback side
                        unsigned char* encTmp;
                        encTmp = receivedBufferReady;
                        receivedBufferReady = receivedBufferProcessing;
                        receivedBufferProcessing = encTmp;
                        //If there is another thread in execution, wait for it
                        if (!first) {
                                pthread_join(callbackThread,NULL);
                        } else {
                                first = false;
                        }
                        pthread_create(&callbackThread,NULL,callbackLauncher,reinterpret_cast<void*>(this));
                }
        }
}

void* Bluetooth::callbackLauncher(void* arg){
        reinterpret_cast<Bluetooth*>(arg)->execCallback();
        return (void*)0;
}

void Bluetooth::execCallback() {
        callbackBuffer(receivedBufferReady, bufferSize);
}

/*
 * This function wait for the button to start the discovery.
 * If the connection is successfully terminated, exit.
 * Otherwise wait for a new button push.
 */
void Bluetooth::waitingForButton(){
        debug("Function waiting for button active!");
        while(state==NOT_CONNECTED) {
                debug("Waiting button push...");
                waitForButton();
                if(state!=NOT_CONNECTED) {
                        debug("WARNING: Already connected!");
                        return;
                }
                if(!isDiscovering) {
                        debug("Launching discovery...");
                        isDiscovering = true;
                        led.startAnimation(kLedAnimationWait);
                        swrite << AT_DISCOVERY << endl;
                }
                Lock<FastMutex> l(discoveryMutex);
                while(isDiscovering) {
                        buttonWaitCV.wait(l);
                }
                debug("Discovery finish");
        }
        //Connection success
        connectionCV.broadcast();
        debug("Function waiting for button finish!");
}

//This thread is waiting every incoming data
void* Bluetooth::doWaitingForRead(void* arg){
        reinterpret_cast<Bluetooth*>(arg)->waitingForRead();
        return (void*)0;
}
//This thread wait for the first push of the button to start discovery
void* Bluetooth::doWaitingForButton(void* arg){
        reinterpret_cast<Bluetooth*>(arg)->waitingForButton();
        return (void*)0;
}

void Bluetooth::connect(){
        debug("Start connection");
        if(state!=NOT_CONNECTED) {
                debug("WARNING: already connected!");
                return;
        }
        debug("Creating threads");
        Thread::create(&Bluetooth::doWaitingForRead,2048,1,(void*)this,Thread::JOINABLE);
        configureButtonInterrupt();
        Thread::create(&Bluetooth::doWaitingForButton,2048,1,(void*)this,Thread::JOINABLE);

        Lock<FastMutex> l(connectionMutex);
        while(state==NOT_CONNECTED) {
                connectionCV.wait(l);
        }
        led.turnOff(kLedRed);
        led.turnOn(kLedBlue);
        debug("Connected successfully!");
}

void Bluetooth::reset(){
        debug("Reset bluetooth...");
        resetBluetooth::high();
        resetBluetooth::mode(Mode::OUTPUT);
        resetBluetooth::low();
        delayMs(200);
        resetBluetooth::high();
        debug("done!");
}

void Bluetooth::initBT(const string& auxPath) {
        debug("Init BT...");
        debug("Opening UART2");
        fcntl(sread, F_SETFL, O_NONBLOCK);
        sread=open(auxPath.c_str(), O_RDONLY | O_NONBLOCK);
        swrite.open(auxPath.c_str());
        if (sread == -1 || !swrite.is_open())
        {
                cout<<"[BLUETOOTH] Error opening USART2"<<endl;
                exit(0);
        }
        //read AT-AB -CommandMode-
        getLineString();
        //read AT-AB BDAddress -xxxx-
        getLineString();
        delayMs(5000);
        debug("Changing bt name...");
        //change name to the BT
        swrite.flush();
        swrite << AT_LOCAL_NAME << BT_NAME << endl;
        //read LocalName
        getLineString();
        debug("done!");
}

string Bluetooth::getLineString(){
        char * buff = new char[1];
        string data="";
        while(read(sread,buff,1)>0 && buff[0]!='\n') {
                data.append(1,buff[0]);
        }
        return data;
}

void Bluetooth::getBuffer(){
        unsigned int i = 0;
        while(read(sread,receivedBufferProcessing+i,1)>0 && ++i<bufferSize) ;
}

Bluetooth& Bluetooth::instance()
{
        static Bluetooth singleton;
        return singleton;
}
Bluetooth::~Bluetooth() {
        swrite.close();
        close(sread);
}
