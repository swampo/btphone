
#ifndef BUTTON_H
#define BUTTON_H

void configureButtonInterrupt();

/** This function returns when it is pressed theblue button */
void waitForButton();

#endif //BUTTON_H
