#include <iostream>
#include <sstream>
#include "miosix.h"
#include "audio/adpcm_player.h"
#include "audio/adpcm_microphone.h"
#include "bluetooth/bluetooth.h"
#include "bluetooth/chat.h"
#include "audio/bell.h"
#include "button/button.h"
#include <tr1/functional>

using namespace std::tr1;
using namespace std;
using namespace miosix;

//If want use debug mode, compile with "make debug"
#ifdef DEBUG
#define debug(str) do { std::cout << "[MAIN] " << str << std::endl; } while( false )
#else
#define debug(str) do { } while ( false )
#endif

Bluetooth& bt = Bluetooth::instance();
ADPCMPlayer& player = ADPCMPlayer::instance();
ADPCMSound receivedSound;

//nSamples: the number of taken samples per buffer
static const unsigned int nSamples = 68 * 256;
//UndersamplingFactor: take one sample each X samples
static const unsigned int undersamplingFactor = 4;
//ADPCM encode 2 samples in 1 byte.
//With the undersampling we obtain 2*X samples in 1 byte.
static const unsigned int samplesPerByte = 2 * undersamplingFactor;
//The bytes buffer dimension
static const unsigned int encodedBufferSize = nSamples/samplesPerByte;

/*
 *	CALLBACK FUNCTIONS
 */
//To use with the received buffer containing the sound
void btCallbackBuffer(const unsigned char * buffer, unsigned int size){
	receivedSound.init(buffer,size, undersamplingFactor);
	player.play(receivedSound);
}
//Executed by the microphone when nSamples has been sampled
void micCallback(Sound& sound){
	const unsigned char * buffer = sound.getBuffer();
	const unsigned int size = sound.getSize();
	bt.sendBuffer(buffer, size);
	//To reproduce the sound locally
	//player.play(sound);
}

//Send bell audio
void sendBell() {
        //Send bell_bin at max size possible wrt the current buffer size intialized
        //Otherwise the receiver, at the last chunk will be waiting other bytes that are ot available
        int len = bell_bin_len / encodedBufferSize;
        len *= encodedBufferSize;
        bt.sendBuffer(bell_bin,len);
}

int main()
{
	debug("System started");

	cout << "BTPHONE 2016..... ENJOY THE BTSIDE!!!" << endl;

	// OVERRIDE: mode talk
	int mode = 2;

	string input;
	while(!(mode==1 || mode==2)){
		cout << "What do you want?" << endl;
		cout << "1 > Chat" << endl;
		cout << "2 > Talk" << endl;
		getline(cin,input);
		 // This code converts from string to number safely.
		 stringstream inStream(input);
		 inStream >> mode;
	}

	if(mode==1){
		debug("Mode CHAT selected");
		Chat& chat = Chat::instance();
		chat.init();
		string data;
		while(chat.isActive()){
			chat.start();
			debug("Entering in chat...");
			while(chat.isChatting()){
				//Read input
				getline(cin,data);
				//Send over the chat
				chat.send(data);
			}
			debug("Chat quitted");
		}
		debug("Chat terminated");
	}
	else if(mode==2){
		debug("Mode TALK selected");

		debug("Bluetooth initializing...");
		bt.init("/dev/auxtty", bind(&btCallbackBuffer,placeholders::_1, placeholders::_2), encodedBufferSize);
		debug("done!");
		debug("Bluetooth connecting...");
		bt.connect();
		debug("done!");

		//Init player and mic
		debug("Player initializing...");
		player.init();
		debug("done!");

		debug("Microphone initializing...");
		ADPCMMicrophone& mic = ADPCMMicrophone::instance();
		mic.init(bind(&micCallback,placeholders::_1),nSamples, undersamplingFactor);
		debug("done!");

		sendBell();
		delayMs(3000);

		mic.start();

		//Wait untile button to close the connection
		waitForButton();
		//Stop the microphone
		mic.stop();
		//Stop player
		player.close();
		//Close the bluetooth connection
		bt.disconnect();
	}

	debug("Terminated");
	return 0;
}
