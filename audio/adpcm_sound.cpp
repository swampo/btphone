/***************************************************************************
*   Copyright (C) 2011 by Terraneo Federico                               *
*   Edited by Redaelli, Riccardi, Salmoiraghi, Turri                      *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   As a special exception, if other files instantiate templates or use   *
*   macros or inline functions from this file, or you compile this file   *
*   and link it with other works to produce a work based on this file,    *
*   this file does not by itself cause the resulting work to be covered   *
*   by the GNU General Public License. However the source code for this   *
*   file must still be made available in accordance with the GNU General  *
*   Public License. This exception does not invalidate any other reasons  *
*   why a work based on this file might be covered by the GNU General     *
*   Public License.                                                       *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, see <http://www.gnu.org/licenses/>   *
***************************************************************************/

#include "miosix.h"
#include <algorithm>
#include "adpcm.h"
#include "adpcm_sound.h"

using namespace std;
using namespace miosix;

//
// class ADPCMSound
//
const unsigned char* ADPCMSound::getBuffer(){
  return soundData;
}

unsigned int ADPCMSound::getSize(){
  return soundSize;
}

bool ADPCMSound::fillMonoBuffer(unsigned short *buffer, int size)
{
        if(size & 0x1) return true;  //Size not divisible by 2
        int remaining=soundSize-index;
        int length=std::min(size,remaining*2);
        for(int i=0; i<length; i+=samplesPerByte)
        {
                /*
                 * Double a sample
                 */
                 unsigned char in=soundData[index++];
                 unsigned short buff = ADPCM_Decode(in & 0xf);
                 for(unsigned int k=0; k<samplesPerByte/2; k++){
                   buffer[i+k]=buff;
                 }
                 buff = ADPCM_Decode(in>>4);
                 for(unsigned int k=samplesPerByte/2; k<samplesPerByte; k++){
                   buffer[i+k]=buff;
                 }
        }
        if(index<soundSize) return false;
        for(int i=length; i<size; i++) buffer[i]=0;
        return true;
}
bool ADPCMSound::fillStereoBuffer(unsigned short *buffer, int size)
{
        if(size & 0x3) return true;  //Size not divisible by 4
        int remaining=soundSize-index;
        int length=std::min(size,remaining*4);
        for(int i=0; i<length; i+=2*samplesPerByte)
        {
                /*
                 * Double a sample... stereo
                 */
                unsigned char in=soundData[index++];
                unsigned short buff = ADPCM_Decode(in & 0xf);
                for(unsigned int k=0; k<samplesPerByte; k++){
                  buffer[i+k]=buff;
                }
                buff = ADPCM_Decode(in>>4);
                for(unsigned int k=samplesPerByte; k<samplesPerByte*2; k++){
                  buffer[i+k]=buff;
                }
        }
        if(index<soundSize) return false;
        for(int i=length; i<size; i++) buffer[i]=0;
        return true;
}

void ADPCMSound::rewind() {
        index=0;
}

void ADPCMSound::init(const unsigned char *data, int size, unsigned int undersamplingFactor){
        //ADPCM encode 2 samples in 1 byte.
        //With the undersampling we obtain 2*X samples in 1 byte.
        samplesPerByte = 2 * undersamplingFactor;
        soundData=data;
        soundSize=size;
        index=0;
}

ADPCMSound::ADPCMSound(){
}
