/***************************************************************************
*   Copyright (C) 2011 by Terraneo Federico                               *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   As a special exception, if other files instantiate templates or use   *
*   macros or inline functions from this file, or you compile this file   *
*   and link it with other works to produce a work based on this file,    *
*   this file does not by itself cause the resulting work to be covered   *
*   by the GNU General Public License. However the source code for this   *
*   file must still be made available in accordance with the GNU General  *
*   Public License. This exception does not invalidate any other reasons  *
*   why a work based on this file might be covered by the GNU General     *
*   Public License.                                                       *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, see <http://www.gnu.org/licenses/>   *
***************************************************************************/

#include "miosix.h"
#include "adpcm_sound.h"

#ifndef ADPCM_PLAYER_H
#define ADPCM_PLAYER_H

/**ADPCMPlayer
 * Class to play an audio file on the STM32's DAC
 */
class ADPCMPlayer
{
public:
/**
 * \return an instance of the player (singleton)
 */
        static ADPCMPlayer& instance();

/**
 * Play an audio file, returning after the file has coompleted playing
 * \param Sound sound file to play
 */
        void play(Sound& sound);

/**
 * \return true if the resource is busy
 */
        bool isPlaying() const;

/**
 * Player initialization
 */
        void init();

        /**
         * Player shutdown
         */
        void close();


private:
        ADPCMPlayer();
        ~ADPCMPlayer();
        ADPCMPlayer(const ADPCMPlayer &);
        ADPCMPlayer& operator= (const ADPCMPlayer &);

        mutable miosix::Mutex mutex;
};

#endif //ADPCM_PLAYER_H
