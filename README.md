# BT Phone #

## Introduction ##
The goal of the project is the development of an inter-phone system, based on Bluetooth radio transmission. We were provided two *STM32F407 DISCOVERY* boards with two Bluetooth modules to be connected.
The two boards, when turned on and connected each other, should be able to allow bidirectional audio communication between the two devices, i.e. transmitting over Bluetooth the voice recorded by the microphone of a board, allowing the other to listen to it through the headphones connected to the jack and viceversa.

![1.jpg](https://bitbucket.org/repo/46gB55/images/4068517142-1.jpg)

## Hardware ##
### Board ###
For this project we used two *STM32F407 DISCOVERY* boards. In our scenario we used USB to power supply the two boards.


The pins we used are the following:

* **GND**: the ground for power supply the Bluetooth module

* **3V**: supply output for the Bluetooth module

* **PA1**: con gured as output to reset the Bluetooth module

* **PA2**: TX pin for serial communication with Bluetooth module

* **PA3**: RX pin for serial communication with Bluetooth module


Optionally, for debug purposes, it is possible to connect a serial port to log on the terminal, by using the following pins:

* **PB10**: TX pin of serial standard output

* **PB11**: RX pin of serial standard input

### Bluetooth module ###
The Bluetooth module we used is *STM SPBT2632C2A*.
The module has been soldered on a stripboard and communicates with the board with the RX/TX serial interface, it is power supplied with 3V and we used the RESET pin during boot procedure to reset the bluetooth at the initial state.
The module is configurable by a set of *AT commands* that must be sent to the serial interface. Once that the connection with a Bluetooth device has been established, it is put into "bypass mode": data sent to the serial interface is no longer interpreted as a command, but it is sent over the wireless channel. It is possible to use an escape sequence to go back in "command mode", where data sent to serial is interpreted again as a command, for example it can be useful for disconnection.

### Wiring ###
![3.png](https://bitbucket.org/repo/46gB55/images/128618494-3.png)
These are the wirings connections between the bluetooth module and the board, as shown in the  figure:

* Vin to 3V

* GND to GND

* RESET to PA1

* RXD to PA2

* TXD to PA3


## Firmware ##
The firmware has been written in C++, on the Miosix operating system (https://miosix.org).

### High level functionalities ###
When the board, properly connected to the Bluetooth module, is turned on, the first task is to initialize the Bleutooth module in order to be able to establish a connection with the other module connected to the other board. During this booting phase, the red LED blinks; when the red LED stays on, it means that the Bluetooth is ready for connection.

Now the user can push the blue button on one of the board (it doesn't matter which, the code is symmetric) and the Bluetooth starts the discovery phase in which scans devices currently available. During this phase there is an animation of the LEDs that seems like a rotating wheel, it lasts about *10s*.
If the other BTPhone is found, there is an animation made with LEDs in which all of them blink for a few times and, if the connection is successful, the blue LED turns on (on both boards). Otherwise, if the other BTPhone cannot be found, the system goes back into the "ready for connection" state, in which the red LED is turned on.

When the connection with the other BTPhone is established (blue LED on), it is played a "bell" and the user can start the communication with the other board by speaking at the microphone. The recorded audio is transmitted over Bluetooth and played on the other board.

To terminate communication, it is sufficient to push the blue button again on any of the two boards: the blue LED turns off, the communication is stopped, the boards disconnect each other and the program is terminated.

To reset the system, push in any moment the black button on both the boards.

To summarize, the various states of the system are signaled by LEDs as follows:

* Red LED blinking: the system is booting and initializing the Bluetooth module; it lasts about *5s*, if it takes more time, it may happen that the Bluetooth module is not properly connected to the board

* Red LED on: the system is ready for connection

* Blue LED on: the system is connected to the other BTPhone and communication is active, it is possible to talk

* Rotating wheel animation: searching other Bluetooth devices

* Blink all animation: other BTPhone found, trying to establish connection