#ifndef LED_H
#define LED_H

#include "miosix.h"
#include "led_constants.h"

using namespace miosix;
using namespace std;

class Led {
public:
    static Led& instance();
    void turnOn(int color);
    void turnOff(int color);
    void startBlink(int color);
    void stopBlink(int color);
    void startAnimation(int animation);
    void stopAnimation();

private:
    Led();
    virtual ~Led();
    Led(const Led &other);
    Led& operator=(const Led &other);
    void setBlinkingBool(int color, bool value);
    Thread *thread_red;
    Thread *thread_blue;
    Thread *thread_green;
    Thread *thread_orange;
    Thread *thread_animation;
    Mutex mutex_red;
    Mutex mutex_blue;
    Mutex mutex_green;
    Mutex mutex_orange;
    Mutex mutex_animation;
};

#endif //LED_H
